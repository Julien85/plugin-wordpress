<?php
/**
 * Plugin Name:       My Plugin test
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Julien Mesnil
**/
 add_action("get_footer", "add_my_phrase");
 function add_my_phrase() {
     echo'youpiiii';
 }

 add_filter("the_content", "add_emoji");
 function add_emoji($content){
     return '&#128299;'.$content;
 }

 add_shortcode("dice", "dice_shortcode");
 function dice_shortcode(){
     return rand(1,6);
 }

  add_shortcode("table", "multiplication");
    function multiplication(){
        $table = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        $nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        $x = "";       
        foreach($table as $nb){
            foreach($nombres as $nb2){
                $x.= $nb . 'x' . $nb2 . '=' . ($nb*$nb2) . '<br>';
            }
        }return $x;
    }
